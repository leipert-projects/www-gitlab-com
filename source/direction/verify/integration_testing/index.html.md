---
layout: markdown_page
title: "Category Direction - Integration Testing"
---

- TOC
{:toc}

## Integration Testing

It's important to validate that the components of your system work together well. By performing integration tests as part of your CI pipeline, you help ensure quality in every build.

- [Issue List](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Category%3A%3AIntegration%20Testing)
- [Overall Vision](/direction/verify)
- [UX Research](https://gitlab.com/groups/gitlab-org/-/epics/592)

Interested in joining the conversation for this category? Please join us in our
[public epic](https://gitlab.com/groups/gitlab-org/-/epics/TBD) where
we discuss this topic and can answer any questions you may have. Your contributions
are more than welcome.

This page is maintained by the Product Manager for Testing, James Heimbuck ([E-mail](mailto:jheimbuck@gitlab.com))

## What's Next & Why

Next up will be [gitlab#37125](https://gitlab.com/gitlab-org/gitlab/issues/37125) an MVC to display how Integration tests (Jmeter, postman, etc.) results have changed as a result of the Merge Request that data is not readily available to the author of the change or anyone reviewing and approving the change.

## Maturity Plan

This category is currently at the "Planned" maturity level and our next target is Minimal (See our [definitions of maturity levels](https://about.gitlab.com/direction/maturity/)).

We are currently doing user research to determine what opportunities exist to reach the minimal level.

## Competitive Landscape

### API testing tools

API testing tools like [SoupUI Pro](https://www.soapui.org/professional/soapui-pro.html), [Apache JMeter](https://jmeter.apache.org/)
and [Hoverfly](https://hoverfly.io/) allows users to automate API and microservices testing across multiple services.

## Top Customer Success/Sales Issue(s)

TBD

## Top Customer Issue(s)

TBD

## Top Internal Customer Issue(s)

TBD

## Top Vision Item(s)

TBD

